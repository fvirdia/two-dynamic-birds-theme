var currentTheme = '';

const themes = {
  day: {
    images: {
      theme_frame: "twolbs_light.jpg"
    },
    colors: {
      frame: "#0d0d0d",
      tab_background_text: "#080808"
    }
  },
  night: {
    images: {
      theme_frame: 'twolbs_dark.jpg',
    },
    colors: {
      button_background_active: "rgb(40, 42, 54)",
      button_background_hover: "rgb(98, 114, 164)",
      frame: "rgb(40, 42, 54)",
      icons: "rgb(248, 248, 242)",
      icons_attention: "rgb(189, 147, 249)",
      ntp_background: "rgb(40, 42, 54)",
      ntp_text: "rgb(248, 248, 242)",
      popup: "rgb(40, 42, 54)",
      popup_border: "rgb(113, 75, 165)",
      popup_highlight: "rgb(68, 71, 90)",
      popup_highlight_text: "rgb(248, 248, 242)",
      popup_text: "rgb(255, 255, 255)",
      sidebar: "rgb(40, 42, 54)",
      sidebar_border: "rgb(98, 114, 164)",
      sidebar_highlight: "rgb(68, 71, 90)",
      sidebar_highlight_text: "rgb(248, 248, 242)",
      sidebar_text: "rgb(248, 248, 242)",
      tab_background_separator: "rgb(98, 114, 164)",
      tab_background_text: "rgb(199, 202, 210)",
      tab_line: "rgb(139, 105, 190)",
      tab_loading: "rgb(98, 114, 164)",
      tab_selected: "rgb(68, 71, 90)",
      tab_text: "rgb(248, 248, 242)",
      toolbar: "rgb(68, 71, 90)",
      toolbar_bottom_separator: "rgb(68, 71, 90)",
      toolbar_field: "rgb(53, 57, 86)",
      toolbar_field_border: "rgb(98, 114, 164)",
      toolbar_field_border_focus: "rgb(113, 75, 165)",
      toolbar_field_highlight: "rgb(98, 114, 164)",
      toolbar_field_highlight_text: "rgb(248, 248, 242)",
      toolbar_field_separator: "rgb(98, 114, 164)",
      toolbar_field_text: "rgb(255, 255, 255)",
      toolbar_text: "rgb(255, 255, 255)",
      toolbar_top_separator: "rgb(40, 42, 54)",
      toolbar_vertical_separator: "rgb(98, 114, 164)"
    }
  }
};

function toggleTheme() {
  if (currentTheme === "day") {
    setTheme("night");
  } else {
    setTheme("day");
  }
}

function setTheme(theme) {
  if (currentTheme === theme) {
    // No point in changing the theme if it has already been set.
    return;
  }
  currentTheme = theme;
  browser.theme.update(themes[theme]);
  if (theme === "day") {
    // browser.browserAction.setTitle({ title: "🕶️ 🌑 🌕 🌘 🌙 Use dark mode." });
    browser.browserAction.setTitle({ title: "🌑" });
  } else {
    // browser.browserAction.setBadgeText({ text: "LOL" });
    browser.browserAction.setTitle({ title: "☀️" });
  }
}

function checkTime() {
  let date = new Date();
  let hours = date.getHours();
  // Will set the sun theme between 8am and 8pm.
  if ((hours > 8) && (hours < 20)) {
    setTheme('day');
  } else {
    setTheme('night');
  }
}

// On start up, check the time to see what theme to show.
checkTime();

// Set up an alarm to check this regularly.
// browser.alarms.onAlarm.addListener(checkTime);
// browser.alarms.create('checkTime', {periodInMinutes: 5});

browser.browserAction.onClicked.addListener(toggleTheme);
