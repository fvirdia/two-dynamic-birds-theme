# Two Dynamic Birds

This theme is based on the Two Little Birds [1] and the Dracula Theme for Thunderbird [2].
Both these works are distributed under the CC BY-SA 3.0 license [3], and as such this work is also under the same license.

Get the theme from https://addons.thunderbird.net/En-Us/thunderbird/addon/two-dynamic-birds/

1. https://addons.thunderbird.net/En-uS/thunderbird/addon/two-litle-birds/
2. https://addons.thunderbird.net/En-uS/thunderbird/addon/dracula-theme-for-thunderbird/
3. https://creativecommons.org/licenses/by-sa/3.0/

## License

This work is published under the [Creative Commons BY-SA 3.0 license](https://creativecommons.org/licenses/by-sa/3.0/).
